﻿using System;

namespace SmartParking.Core
{
    /// <summary>
    /// Represent a single hardware device that manage a proximity sensor. 
    /// Implementation of this class should be specific for a single hardware.
    /// </summary>
    internal interface IPhysicalDevice
    {
        /// <summary>
        /// Initialize the Device and create a connection to it
        /// </summary>
        void Init();

        /// <summary>
        /// Callback when the hardware device is connected and ready
        /// </summary>
        event EventHandler<object> Ready;

        /// <summary>
        /// Callback when the connection with the device is lost
        /// </summary>
        event EventHandler<object> ConnectionLost;

        /// <summary>
        /// Callback when a status has been sent from the physical device.
        /// </summary>
        event EventHandler<StatusReceivedArgs> StatusReceived;
    }
}
