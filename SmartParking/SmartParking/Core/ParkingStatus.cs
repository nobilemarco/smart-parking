﻿

namespace SmartParking.Core
{
    /// <summary>
    /// Enumerate the possible status of a single parking
    /// </summary>
    enum ParkingStatus
    {
        Occupied, Free
    }
}
