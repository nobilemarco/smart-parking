﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SmartParking.Core
{
    /// <summary>
    /// Class that manages the status of the entire parking. Receive the callbacks from all
    /// the single parkings and update itself. Provides publically method to obtain this status.
    /// </summary>
    class ParkingController
    {

        private volatile List<SingleParking> _connectedParkings;

        private static ParkingController _instance;

        private volatile SortedDictionary<int, ParkingStatus> _generalStatus;

        private ParkingController()
        {
            _generalStatus = new SortedDictionary<int, ParkingStatus>();
            _connectedParkings = new List<SingleParking>();
        }

        /// <summary>
        /// Singleton of this class
        /// </summary>
        public static ParkingController Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = new ParkingController();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Method to connect a new device(provided of sensor) to the system
        /// </summary>
        /// <param name="parkingNumber">Number assigned to new single parking</param>
        /// <param name="ipAddress">Ip address of remote physical device</param>
        public void ConnectRemoteDeviceAsParking(int parkingNumber, string ipAddress)
        {
            SingleParking singleParking = new SingleParking(parkingNumber, ipAddress,
                    PhysicalDeviceType.Arduino);
            //Handle the switching of a single parking status
            singleParking.StatusChanged += SingleParkingStatusChanged;
            singleParking.SingleParkingReady += SingleParkingReady;
            singleParking.DeviceLost += DeviceLost;
            _connectedParkings.Add(singleParking);
        }

        /// <summary>
        /// Action performed when a device has been lost. Remove that single parking from model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeviceLost(object sender, EventArgs e)
        {
            _generalStatus.Remove(((SingleParking) sender).ParkingNumber);
        }

        /// <summary>
        /// Action performed when a device has been connected and ready. 
        /// Add that single parking to model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SingleParkingReady(object sender, EventArgs e)
        {
            _generalStatus.Add(((SingleParking)sender).ParkingNumber, ((SingleParking)sender).Status);
            Debug.WriteLine("Single parking " + ((SingleParking)sender).ParkingNumber + " added to status");
        }

        /// <summary>
        /// Method to get the number of free places in the parking
        /// </summary>
        /// <returns>number of free single parkings</returns>
        public int GetFreePlaces()
        {
            return CalculateFreeParkingsNumber();
        }

        /// <summary>
        /// Method to get the global current status of the parking
        /// </summary>
        /// <returns>A dictionary containing the global status</returns>
        public SortedDictionary<int, ParkingStatus> GetCurrentStatus()
        {
            return _generalStatus;
        }

        /// <summary>
        /// Start a global checking of devices connections
        /// </summary>
        public void CheckParkingConnections()
        {
            foreach (SingleParking parking in _connectedParkings)
            {
                parking.CheckConnectionWithDevice();
            }
        }

        /// <summary>
        /// Action performed when a single parking changed his status.
        /// Update the model with the new information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SingleParkingStatusChanged(object sender, SingleParking.SingleParkingNumber e)
        {
            Debug.WriteLine("Controller status changed received: " + e.Number);

            ParkingStatus previousStatus = _generalStatus[e.Number];
            switch (previousStatus)
            {
                case ParkingStatus.Free:
                    _generalStatus[e.Number] = ParkingStatus.Occupied;
                    break;
                case ParkingStatus.Occupied:
                    _generalStatus[e.Number] = ParkingStatus.Free;
                    break;
            }
        }


        /// <summary>
        /// Method to get the number of free places from the global status
        /// </summary>
        /// <returns></returns>
        private int CalculateFreeParkingsNumber()
        {
            return _generalStatus.Count(singleParking => singleParking.Value == ParkingStatus.Free);
        }

    }
}
