﻿using System;

namespace SmartParking.Core
{
    /// <summary>
    /// Args of a STatusReceived callback
    /// </summary>
    internal class StatusReceivedArgs : EventArgs
    {
        private string _message;

        /// <summary>
        /// message received from the physical device
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
    }
}
