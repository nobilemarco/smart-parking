﻿
namespace SmartParking.Core
{
    /// <summary>
    /// Enumeration of possible physical devices used in the Parking to manage the proximity sensors
    /// </summary>
    enum PhysicalDeviceType
    {
        Arduino
    }
}
