﻿using System;
using System.Diagnostics;
using Windows.Networking;
using Microsoft.Maker.Firmata;
using Microsoft.Maker.RemoteWiring;
using Microsoft.Maker.Serial;

namespace SmartParking.Core
{
    /// <summary>
    /// Implementation of IPhysicalDevice specific for Arduino device.
    /// </summary>
    class ArduinoDevice : IPhysicalDevice
    {
        private const int DefaultArduinoConnectionPort = 5000;

        public event EventHandler<object> Ready;
        public event EventHandler<object> ConnectionLost;
        public event EventHandler<StatusReceivedArgs> StatusReceived;

        private readonly string _ipAddress;
        private RemoteDevice _arduino;
        private NetworkSerial _connection;
        private UwpFirmata _firmata;

        public ArduinoDevice(string ipAddress)
        {
            _ipAddress = ipAddress;
        }

        public void Init()
        {
            _connection = new NetworkSerial(new HostName(_ipAddress), DefaultArduinoConnectionPort);
            _firmata = new UwpFirmata();
            _arduino = new RemoteDevice(_firmata);
            _firmata.begin(_connection);
            _connection.begin(9800, SerialConfig.SERIAL_8N1);
            _connection.ConnectionEstablished += ConnectionEstablished;
            _arduino.DeviceConnectionLost += OnConnectionLost;
            _arduino.StringMessageReceived += ArduinoOnStringMessageReceived;
        }

        /// <summary>
        /// Action performed when the connection is lost
        /// </summary>
        /// <param name="message">message received from the physical device through the connection</param>

        private void OnConnectionLost(string message)
        {
            Debug.Write("Arduino Lost");
            ConnectionLost?.Invoke(this, null);
        }

        /// <summary>
        /// Action performed when a message come from the device. The message is sent to eventuals
        /// upper layers through a callback 
        /// </summary>
        /// <param name="message">message received from the physical device through the connection</param>
        private void ArduinoOnStringMessageReceived(string message)
        {
            StatusReceivedArgs receivedArgs = new StatusReceivedArgs();
            receivedArgs.Message = message;
            StatusReceived?.Invoke(this, receivedArgs);
        }

        /// <summary>
        /// Action performed when the connection has been created with the device
        /// </summary>
        private void ConnectionEstablished()
        {
            Debug.Write("Arduino ready");
            Ready?.Invoke(this, null);
        }
    }
}
