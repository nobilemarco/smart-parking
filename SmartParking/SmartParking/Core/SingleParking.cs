﻿using System;
using System.Diagnostics;

namespace SmartParking.Core
{
    /// <summary>
    /// Represent an abstract single parking. 
    /// </summary>
    class SingleParking
    {
        /// <summary>
        /// Callback method called when the status of the single parking has changed
        /// </summary>
        public event EventHandler<SingleParkingNumber> StatusChanged;

        /// <summary>
        /// Callback method called when the single parking is ready to receive inputs from hardware
        /// </summary>
        public event EventHandler SingleParkingReady;

        /// <summary>
        /// Callback method called when the connection with the physical device has been lost
        /// </summary>
        public event EventHandler DeviceLost;

        private const int MaxTimeoutBeforeConnectionLost = 20;
        private ParkingStatus _status = ParkingStatus.Free;
        private readonly IPhysicalDevice _device;
        private bool _ready;
        private DateTime _lastUpdate = DateTime.UtcNow;

        /// <summary>
        /// Return is the single parking is connected to the physical device
        /// </summary>
        public bool Ready
        {
            get { return _ready; }
        }

        /// <summary>
        /// Return the status of the single parking (Free or Occupated)
        /// </summary>
        public ParkingStatus Status
        {
            get { return _status; }
        }

        /// <summary>
        /// Return the single parking's number
        /// </summary>
        public int ParkingNumber { get; }

        public SingleParking(int number, string ipAddress, PhysicalDeviceType type)
        {
            ParkingNumber = number;

            switch (type)
            {
                case PhysicalDeviceType.Arduino:
                    _device = new ArduinoDevice(ipAddress);
                    _device.Ready += OnDeviceReady;
                    _device.ConnectionLost += OnConnectionLost;
                    _device.StatusReceived += OnStatusReceived;
                    _device.Init();
                    break;

                //Insert here other type and assign an eventual custom implementation of
                //IPhysicalDevice
            }
        }

        /// <summary>
        /// Check the connection with the device assigned to the single parking.
        /// If it results lost, reinits it.
        /// </summary>
        public void CheckConnectionWithDevice()
        {
            TimeSpan diff = DateTime.UtcNow - _lastUpdate;
            if (diff.Seconds >= MaxTimeoutBeforeConnectionLost)
            {
                OnConnectionLost(this, null);
            }
        }

        /// <summary>
        /// Action performed when the connection with the device has been lost
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnConnectionLost(object sender, object e)
        {
            Debug.WriteLine("Arduino Lost");
            _ready = false;
            _lastUpdate = DateTime.UtcNow;
            DeviceLost?.Invoke(this, EventArgs.Empty);
            _device.Init();
        }

        /// <summary>
        /// Action performed when a status has been received from the device.
        /// Check if the status received is the same of current status, if not, change the
        /// current status and send a callback to the listeners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStatusReceived(object sender, StatusReceivedArgs e)
        {
            Debug.WriteLine("SingleParking received: " + e.Message);
            
            _lastUpdate = DateTime.UtcNow;
            bool occupied;
            if (e.Message.Equals("o"))
            {
                occupied = true;
            }
            else
            {
                occupied = false;
            }

            switch (_status)
            {
                case ParkingStatus.Free:
                    if (occupied)
                    {
                        _status = ParkingStatus.Occupied;
                        SingleParkingNumber singleParkingNumber = new SingleParkingNumber { Number = ParkingNumber };
                        StatusChanged?.Invoke(this, singleParkingNumber);
                    }
                    break;
                case ParkingStatus.Occupied:
                    if (!occupied)
                    {
                        _status = ParkingStatus.Free;
                        SingleParkingNumber singleParkingNumber = new SingleParkingNumber { Number = ParkingNumber };
                        StatusChanged?.Invoke(this, singleParkingNumber);
                    }
                    break;
            }
        }

        /// <summary>
        /// Action performed when the physical device is connected and ready
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDeviceReady(object sender, object e)
        {
            _ready = true;
            SingleParkingReady?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>
        /// Args of a callback containing the number of this parking
        /// </summary>
        internal class SingleParkingNumber : EventArgs
        {
            public int Number { get; set; }
        }

    }
}
