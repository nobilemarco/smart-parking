﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.System.Threading;
using SmartParking.Core;
using SmartParking.Web;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace SmartParking
{
    public sealed class StartupTask : IBackgroundTask
    {
        private const int DelayTime = 10000;
        private BackgroundTaskDeferral _deferral;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            _deferral =  taskInstance.GetDeferral();
            List<KeyValuePair<int, string>> customParkingList = new List<KeyValuePair<int, string>>();

            /*********************************************************************/
            /*        CUSTOM PART: HERE THE PARKINGS ARE EMBEDDED MANUALLY       */

            customParkingList.Add(new KeyValuePair<int, string>(1, "192.168.1.81"));
            customParkingList.Add(new KeyValuePair<int, string>(2, "192.168.1.82"));

            /*********************************************************************/

            //Connect all devices to the system
            foreach (KeyValuePair<int, string> pair in customParkingList)
            {
                ParkingController.Instance.ConnectRemoteDeviceAsParking(pair.Key, pair.Value);

            }
            WebServer server = new WebServer();
            await ThreadPool.RunAsync(workItem =>
             {
                 //Starts the web server
                 server.Start();
             });


            while (true)
            {
                ParkingController.Instance.CheckParkingConnections();
                await Task.Delay(DelayTime);
            }
        }
    }
}
