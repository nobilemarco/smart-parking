﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using SmartParking.Core;

namespace SmartParking.Web
{
    /// <summary>
    /// Web server that listen for incoming GET, and provides current status of the parking
    /// </summary>
    class WebServer
    {
        private const uint BufferSize = 8192;
        private const string ListeningPort = "60000";


        /// <summary>
        /// Starts a Web Server
        /// </summary>
        public async void Start()
        {
            StreamSocketListener listener = new StreamSocketListener();

            await listener.BindServiceNameAsync(ListeningPort);

            listener.ConnectionReceived += async (sender, args) =>
            {
                StringBuilder request = new StringBuilder();
                using (IInputStream input = args.Socket.InputStream)
                {
                    byte[] data = new byte[BufferSize];
                    IBuffer buffer = data.AsBuffer();
                    uint dataRead = BufferSize;
                    while (dataRead == BufferSize)
                    {
                        await input.ReadAsync(buffer, BufferSize, InputStreamOptions.Partial);
                        request.Append(Encoding.UTF8.GetString(data, 0, data.Length));
                        dataRead = buffer.Length;
                    }
                }

                using (IOutputStream output = args.Socket.OutputStream)
                {
                    using (Stream response = output.AsStreamForWrite())
                    {
                        StringBuilder builder = new StringBuilder();

                        builder.Append("<html>" +
                                       "<head>" +
                                       "<meta http-equiv=\"refresh\" content=\"5\" >" +
                                       "</head>" +
                                       "<body>");
                        builder.Append(
                            "<div style =\"text-align: center\"><h1><span style = \"font-family:trebuchet ms,helvetica,sans-serif;\">" +
                            " Smart Parking </span></h1>");
                        builder.Append(
                            "<h3><span style = \"font-family:trebuchet ms,helvetica,sans-serif;\">" +
                            " Free Parkings: &nbsp;<span style = \"color:#FF0000;\"> " +
                            ParkingController.Instance.GetFreePlaces() + " </span></span></h3> </div>");
                      

                        builder.Append("<table style = \"border-collapse:collapse; border-spacing:0; border-color:#aaa;" +
                            "border-width:1px;border-style:solid;margin:0px auto;\">" +
                                       "<tr>" +
                                       "<th style = \"font-family:Arial, sans-serif; font-size:14px;" +
                            " font-weight:normal; padding: 10px 5px; border-style:solid;" +
                            " border-width:1px; overflow: hidden; word-break:normal;" +
                            " border-color:#aaa;color:#fff;background-color:#f38630; \">Parking Number</th>" +
                                       "<th style = \"font-family:Arial, sans-serif; font-size:14px;" +
                            " font-weight:normal; padding: 10px 5px; border-style:solid;" +
                            " border-width:1px; overflow: hidden; word-break:normal;" +
                            " border-color:#aaa;color:#fff;background-color:#f38630;\">Status</th>" +
                                       "</tr>");
                        builder.Append(GenerateRowsFromStatus());
                        builder.Append("</table>");
                        builder.Append("</body></html>");

                        byte[] bodyArray = Encoding.UTF8.GetBytes(builder.ToString());
                        var bodyStream = new MemoryStream(bodyArray);

                        var header = "HTTP/1.1 200 OK\r\n" +
                                    $"Content-Length: {bodyStream.Length}\r\n" +
                                        "Connection: close\r\n\r\n";

                        byte[] headerArray = Encoding.UTF8.GetBytes(header);
                        await response.WriteAsync(headerArray, 0, headerArray.Length);
                        await bodyStream.CopyToAsync(response);
                        await response.FlushAsync();
                    }
                }
            };
        }

        /// <summary>
        /// Method to obtain the table's rows from the global status
        /// </summary>
        /// <returns>html string representing the rows of the table</returns>
        private string GenerateRowsFromStatus()
        {
            
            StringBuilder builder = new StringBuilder();


            foreach (KeyValuePair<int, ParkingStatus> status in ParkingController.Instance.GetCurrentStatus())
            {
                builder.Append("<tr><td style = \"font-family:Arial, sans-serif; font-size:14px;" +
                            " padding: 10px 5px; border-style:solid; border-width:1px;" +
                            " overflow: hidden; word-break:normal; border-color:#aaa;" +
                            "color:#333;background-color:#fff;\">" + status.Key +"</td>" +
                               "<td style = \"background-color:#FCFBE3; font-family:Arial, sans-serif; font-size:14px;" +
                            " padding: 10px 5px; border-style:solid; border-width:1px;" +
                            " overflow: hidden; word-break:normal; border-color:#aaa;" +
                            "color:#333;background-color:#fff;\">" + status.Value +"</td></tr>");
            }
            

            return builder.ToString();
        }
    }
}
